<?php

//__construct() & __destruct()

class person{
    public $name;
    public function __construct()
    {
    echo "I'm inside the ".__METHOD__."<br>";
    }
    public function __destruct()
    {
        echo "I am dying, please pray for me.<br>";
    }
}
class person1{
    public $name;

    public function __construct()
    {
        echo "I'm inside the ".__METHOD__."<br>";
    }
    public function __destruct()
    {
        echo "I am dying. I'm inside the ".__METHOD__."<br>";
    }
}
$obj= new Person();
unset($obj);
$obj1= new Person1();


echo "<br>";
echo "<br>";


//__call()

class person2{
    public $name;

    public function doSomething()
    {
        echo "I'm doing something<br>";
    }
    public function __construct()
    {
        echo "I'm inside the ".__METHOD__."<br>";
    }
    public function __destruct()
    {
        echo "I am dying, please pray for me.<br>";
    }
    public function __call($name, $arguments)
    {
        echo "I am inside the ".__METHOD__."<br>";

        echo "name=$name<br>";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
    }
}
$obj= new Person2();
$obj->doSomething();
$obj->habijabi("para_habi1", "para_jabi1");

echo "<br>";
echo "<br>";

//__callStatic() // __get() // __set() //__isset() //__unset() //__sleep() //__wakeup() //__tostring()  //__invoke()

class person3{
    public $name="Lucky";
    public $phone="0167-589547";
    public $dateOfBirth="01-03-2017";

    public static $message;

    public static function doFrequently(){
        echo "I'm doing it frequently...<br>";
    }
    public function doSomething()
    {
        echo "I'm doing something here...<br>";
    }
    public function __construct()
    {
        echo "I'm inside the ".__METHOD__."<br>";
    }
    public function __destruct()
    {
        echo "I am dying, please pray for me.<br>";
    }
    public function __call($name, $arguments)
    {
        echo "I am inside the ".__METHOD__."<br>";

        echo "Wrong name=$name<br>";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
    }
    public static function __calStatic($name, $arguments)
    {
        echo "I am inside the static ".__METHOD__."<br>";

        echo "wrong static name=$name<br>";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
    }
    public function __set($name, $value)
    {
        echo __METHOD__."Wrong property name = $name<br>";
        echo "Value tried to set to that wrong property= $value<br>";
    }
    public function __get($name)
    {
        echo __METHOD__."Wrong Property Name=$name<br>";

    }
    public function __isset($name)
    {
        echo __METHOD__."Wrong Property Name=$name<br>";

    }
    public function __unset($name)
    {
        echo __METHOD__."Wrong Property Name=$name<br>";

    }
    public function __sleep()
    {
        return array("name","phone");

    }
    public function __wakeup()
    {
        $this->doSomething();
    }
    public function __toString()
    {
        return "Are u crazy? Don't forget that I'm an Object.<br>";
    }
    public function __invoke()
    {
        echo "I'am an object but you are trying to call me as a function!<br>";
    }
}  //end of Person3 Class

Person3::$message="Any message here";   //static property setting test
echo Person3::$message."<br>";          //static property reading test

Person3::doFrequently("wrong static para1","wrong static para2");    //Wrong static method call

$obj= new Person3();    //construct
$obj->doFrequently();  //user defined method call

$obj->habijabi("para_habi1", "para_jabi1");     //__call()
$obj->address="5/2 Gol Pahar Mor, Chittagong";  //__set()

echo $obj->address;   //__get()

if(isset($obj->naiProperty)){       //__isset()
    }
unset($obj->naiProperty);           //__unset()

$myVar=serialize($obj);
echo "<pre>";
var_dump($myVar);
echo "</pre>";

echo "<pre>";
var_dump(unserialize($myVar));
echo "</pre>";

echo $obj;          //__toString
$obj();            //__invoke



